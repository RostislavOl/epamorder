package com.epam.order.roolOrder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoolOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoolOrderApplication.class, args);
	}

}
