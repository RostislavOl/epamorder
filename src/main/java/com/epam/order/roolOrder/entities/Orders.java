/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.order.roolOrder.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import javax.persistence.Table;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@Table(name ="orders")
public class Orders implements Serializable {
    
    @Id
    @Column
    private Long id;
    
    @Column(name="goodsName")
    private String goodsName;
    
    @Column(name="eat")
    @Enumerated(EnumType.STRING)
    private catFood category;
    
    @Column
    private Long clientID;
    
    private enum catFood{
        ProPlan, RoyalCanin, Felix
    }
    
}
