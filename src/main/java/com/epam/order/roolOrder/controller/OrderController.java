/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.order.roolOrder.controller;

import com.epam.order.roolOrder.entities.Orders;
import com.epam.order.roolOrder.service.OrdersService;
import java.net.URI;
import java.net.URISyntaxException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    
    private final OrdersService orderService;
    
    @GetMapping("/allOrders")
    public ResponseEntity<?> getOrders(){
        return ResponseEntity.status(HttpStatus.OK).body(orderService.getOrders());
    }
    
    @GetMapping("/allOrders/{orderID}")
    public ResponseEntity<?> getOrderByID(@PathVariable Long orderID){
        return ResponseEntity.status(HttpStatus.OK).body(orderService.getOrderById(orderID));
    }
    
    @PostMapping("/newOrder/")
    public ResponseEntity<?> createOrder(@RequestBody Orders order){
        return ResponseEntity.status(HttpStatus.OK).body(orderService.createOrder(order));
    }
    
    @PutMapping("/update/")
    public ResponseEntity<?> update(@RequestBody Orders order) {
        orderService.updateOrder(order);
        try {
            return ResponseEntity
                    .created(new URI("/allOrders/" + order.getId()))
                    .body(order);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.CREATED).body(order);
        }
    }
    
    @DeleteMapping("/{orderID}")
    public void deleteOrder(@PathVariable Long orderID){
        orderService.deleteOrder(orderID);
    }
}
