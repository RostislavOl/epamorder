/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.order.roolOrder.service;

import com.epam.order.roolOrder.entities.Orders;
import com.epam.order.roolOrder.repository.OrderRepo;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class OrdersService {
    
    private final OrderRepo repository;
    
    public List<Orders> getOrders(){
        return repository.findAll();
    }
    
    public Orders getOrderById(Long id){
        Orders order = repository.findById(id).orElse(new Orders());
        return order;
    }
    
    @Transactional
    public Orders createOrder(Orders order){
        return repository.save(order);
    }
    
    @Transactional
    public void deleteOrder(Long orderID){
        repository.deleteById(orderID);
    }
    
    @Transactional
    public Orders updateOrder(Orders order){
        if (order != null) {
            Orders orderInRepository = getOrderById(order.getId());
            if (orderInRepository != null) {
                repository.deleteById(orderInRepository.getId());
                orderInRepository.setId(order.getId());
                orderInRepository.setCategory(order.getCategory());
                orderInRepository.setClientID(order.getClientID());
                orderInRepository.setGoodsName(order.getGoodsName());
                repository.save(orderInRepository);
                return order;
            } else {
                repository.save(order);
            }
        }
        return null;
    }
    
}
