﻿CREATE DATABASE petshop;
CREATE USER roole WITH password '0816';
GRANT ALL ON DATABASE petshop TO roole;
CREATE SCHEMA epam;


CREATE TYPE catFood AS ENUM ('ProPlan', 'RoyalCanin', 'Felix');

CREATE SEQUENCE order_count;
CREATE TABLE epam.orders
(
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('order_count'),
  goodsName CHAR(160),
  eat catFood,
  clientID BIGINT REFERENCES epam.client(id)
);

CREATE TABLE epam.client_type
(
  id numeric PRIMARY KEY,
  shortName CHAR(60),
  fullName CHAR(255),
  clientTypeCode CHAR(10)
);

CREATE SEQUENCE client_count;
CREATE TABLE epam.client
(
  id numeric PRIMARY KEY DEFAULT NEXTVAL('order_count'),
  shortName CHAR(160),
  fullName CHAR(255),
  clientType numeric REFERENCES epam.client_type(id),
  inn CHAR(12),
  okpo CHAR(10),
  creationDate DATE,
  modificationDate DATE
);



  orders numeric[] REFERENCES epam.orders(id),
